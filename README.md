## A github data analytics web service

This simple web service provides analytics data.

## First time setup

Clone the repo and install the dependencies

```
$ npm install
```

## Running development server

```
$ npm run serve
```

## Example

To get a breakdown on issue labels or state for a given date:

GET "/api/issues?date=[DATE]&criteria=['label' | 'state']"
where DATE is in an ISO 8061 (YYYY-MM-DD) format.

```
$ curl "localhost:5000/api/issues?date=2018-09-10&criteria=state"
```

will give you a json object

```javascript
{"open": 30, "closed": 50}
```
