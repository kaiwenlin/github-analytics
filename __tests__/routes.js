const request = require("supertest");
const app = require("../app");

describe("Test the root path", () => {
  it("should respond with the GET method", async () => {
    const res = await request(app).get("/");
    expect(res.statusCode).toBe(200);
  });

  it("should respond with message ROOT hit", async () => {
    const res = await request(app).get("/");
    expect(res.text).toBe("ROOT hit");
  });
});

describe("Test the error handler", () => {
  it("should respond with 404 for undefined routes", async () => {
    const res = await request(app).get("/badRoute/wubadub");
    expect(res.statusCode).toBe(404);
  });
});

describe("Test the issues endpoint", () => {
  it("should catch missing date and criteria fields", async () => {
    const res = await request(app).get("/api/issues");
    expect(res.statusCode).toBe(422);
    expect(res.body.error.message).toBe("date and criteria fields missing");
  });

  it("should catch non ISO 8601 date input", async () => {
    const res = await request(app).get(
      "/api/issues?date=3920-23-XX&criteria=label"
    );
    expect(res.statusCode).toBe(422);
    expect(res.body.error.message).toBe("date is not ISO 8601 format");
  });

  it("should catch non valid criteria", async () => {
    const res = await request(app).get(
      "/api/issues?date=2018-04-01&criteria=chickens"
    );
    expect(res.statusCode).toBe(422);
    expect(res.body.error.message).toContain("criteria not valid");
  });
});
