const express = require("express");
const bodyParser = require("body-parser");

const errorHandler = require("./handlers/error");
const issuesRoutes = require("./routes/issues");

const app = express();
app.use(bodyParser.json());

/**
 * @route   {GET} /
 * @desc    Root path just to test
 * @access  Public
 */
app.get("/", (req, res) => {
  return res.send("ROOT hit");
});

app.use("/api/issues", issuesRoutes);

app.use(function(req, res, next) {
  let err = new Error("Not Found");
  err.status = 404;
  next(err);
});

app.use(errorHandler);

module.exports = app;
