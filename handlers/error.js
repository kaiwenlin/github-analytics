/**
 *
 * Middleware to catch errors
 *
 * @param {*} error
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function errorHandler(error, req, res, next) {
  return res.status(error.status || 500).json({
    error: {
      message: error.message || "Oops! Something went wrong."
    }
  });
}

module.exports = errorHandler;
