const octokit = require("@octokit/rest")();
const moment = require("moment");

/**
 *
 * Gets issue related analytics via Github API for a given date
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getIssueAnalytics = async function(req, res, next) {
  const LABEL = "label";
  const STATE = "state";

  // Validate HTML query params
  const { date, criteria } = req.query;
  const validCriteria = [LABEL, STATE];
  if (!(date && criteria)) {
    return next({
      status: 422,
      message: "date and criteria fields missing"
    });
  } else if (!moment(date, moment.ISO_8601).isValid()) {
    return next({
      status: 422,
      message: "date is not ISO 8601 format"
    });
  } else if (!validCriteria.includes(criteria)) {
    return next({
      status: 422,
      message: `criteria not valid: ${criteria}. Valid criteria: ${validCriteria}`
    });
  }

  try {
    let githubRes = await octokit.search.issues({
      q: `created=${date}`,
      per_page: 100
    });

    let { items } = githubRes.data;

    while (octokit.hasNextPage(githubRes)) {
      githubRes = await octokit.getNextPage(githubRes);
      items = items.concat(githubRes.data.items);
    }

    const breakdown = items.reduce((accum, issue) => {
      switch (criteria) {
        case LABEL:
          issue.labels.forEach(label => {
            if (accum[label.name]) {
              accum[label.name]++;
            } else {
              accum[label.name] = 1;
            }
          });
          break;
        case STATE:
          if (accum[issue.state]) {
            accum[issue.state]++;
          } else {
            accum[issue.state] = 1;
          }
          break;
        default:
          // Not much we can do but it really shouldn't hit here
          break;
      }
      return accum;
    }, {});

    return res.status(200).json(breakdown);
  } catch (err) {
    return next(err);
  }
};
