const express = require("express");
const router = express.Router();

const { getIssueAnalytics } = require("../handlers/issues");

/**
 * @route   {GET} /api/issues
 * @desc    Gets issues related analytics data via HTML queries
 * @access  Public
 */
router.route("/").get(getIssueAnalytics);

module.exports = router;
